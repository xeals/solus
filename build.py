#!/usr/bin/env python3

import argparse
import os
import sys
import yaml
import ast
from subprocess import run

def info(info_str):
    print("--------------------------------------------\n",
          info_str,
          "\n--------------------------------------------")

def index_pkgs():
    ignored = [
            '.git', '.gitignore', '.gitmodules', 'README.md',
            'clean.sh', 'build.py', 'bootstrap',
            'common', 'Makefile', 'Makefile.common', 'Makefile.iso',

            # Extras that won't work.
            'chapel', 'kitty', 'macos-sierra-gtk-theme',
            # Holy shit what is RAM
            'monero',
            # Elm stuff that's not working ever.
            'elm-compiler',

            'haskell-cabal-install'
            ]

    # These packages are automatically ignored if their dependencies are ignored.
    local_deps = {
            # `haskell-codeworld-api`
            'haskell-wai': [ 'haskell-vault' ],
            'haskell-warp': [ 'haskell-http2', 'haskell-http-date',
                'haskell-wai', 'haskell-simple-sendfile',
                'haskell-iproute'],
            'haskell-scotty': [ 'haskell-wai-extra', 'haskell-warp' ],
            'haskell-wai-logger': [ 'haskell-wai', 'haskell-byteorder' ],
            'haskell-wai-extra': [ 'haskell-wai', 'haskell-word8',
                'haskell-wai-logger', 'haskell-stringsearch',
                'haskell-iproute' ],
            'haskell-kansas-comet': [ 'haskell-scotty' ],
            'haskell-blank-canvas': [ 'haskell-kansas-comet',
                'haskell-colour', 'haskell-wai' ],
            'haskell-cereal-text': [ 'haskell-cereal' ],
            'haskell-codeworld-api': [ 'haskell-blank-canvas', 'haskell-cereal', 'haskell-random-shuffle' ],

            # `haskell-tasty`
            'haskell-tasty': [ 'haskell-optparse-applicative',
                'haskell-unbounded-delays' ],
            'haskell-tasty-hunit': [ 'haskell-tasty' ],
            'haskell-tasty-quickcheck': [ 'haskell-tasty' ],

            # other haskell tools
            'haskell-hindent': [ 'haskell-descriptive', 'haskell-src-exts' ],
            'haskell-doctest': [ 'haskell-code-page' ],
            'haskell-stylish': [ 'haskell-strict' ],

            # `haskell-snap`
            'haskell-io-streams-haproxy': [ 'haskell-io-streams' ],
            'haskell-snap-core': [ 'haskell-io-streams', 'haskell-readable',
                'haskell-bytestring-builder' ],
            'haskell-snap-server': [ 'haskell-snap-core',
                'haskell-io-streams-haproxy' ],
            'haskell-websockets': [ 'haskell-entropy' ],
            'haskell-websockets-snap': [ 'haskell-websockets',
                'haskell-snap-core', 'haskell-snap-server' ],

            # `haskell-language`
            'haskell-testing-feat': [ 'haskell-tagshare' ],
            'haskell-language-ecmascript': [ 'haskell-testing-feat',
                'haskell-charset', 'haskell-Diff', 'haskell-wl-pprint' ],
            'haskell-language-glsl': [ 'haskell-prettyclass' ],

            # elm
            'elm-compiler': [ 'haskell-edit-distance', 'haskell-language-ecmascript',
                'haskell-language-glsl', 'haskell-union-find', 'haskell-indents',
                'haskell-binary' ],
            'elm-package': [ 'elm-compiler', 'haskell-zip-archive',
                'haskell-parallel-io', 'haskell-binary', 'haskell-edit-distance' ],
            'elm-reactor': [ 'elm-compiler', 'elm-package', 'haskell-cmdargs',
                    'haskell-websockets-snap' ],
            'elm-make': [ 'elm-compiler', 'elm-reactor', 'haskell-raw-strings-qq' ],
            'elm-repl': [ 'elm-compiler', 'elm-package', 'haskell-cmdargs',
                    'haskell-bytestring-trie' ],

            # other
            'otfcc': [ 'premake5' ],
            'shards': [ 'crystal' ],
            'wpgtk': [ 'python-olefile' ],
    }

    packages = [p for p in os.listdir()
            if p not in ignored
                and p not in list(local_deps.keys())]

    return packages, ignored, local_deps

def list_eopkgs():
    return [p for p in os.listdir() if p.endswith('.eopkg')]

def cache_build():
    "Stores the last build package name(s) of a given directory."
    pkgs = list_eopkgs()
    with open('.lastbuild', 'w') as f:
        f.write(str(pkgs))

def check_build():
    """Determines if the repo needs rebuilding.
    Returns True if it does."""
    with open('package.yml', 'r') as f: yml = yaml.load(f)
    try:
        f_suffix = list(yml['patterns'][0].keys())[0]
        name = yml['name'] + '-' + f_suffix
    except KeyError:
        name = yml['name']

    pkg = name + '-' + str(yml['version']) + '-' + str(yml['release']) + '-' + '1-x86_64.eopkg'

    try:
        with open('.lastbuild', 'r') as f:
            l = ast.literal_eval(f.read())
            return pkg not in l
    except FileNotFoundError:
        return True

def place_in_repo(repo='/var/lib/solbuild/local', move=False):
    """Copies all resulting packages into the local repo,
    then moves to the parent directory.

    By default, built packages are only copied to the repo.
    Advantages of this are saving recompilation (with the current method of
    determining this). Disadvantages are increased disk space."""
    pkgs = list_eopkgs()

    info("-- Adding packages:\n"
         f"-- {str(pkgs)}")
    op = 'mv' if move else 'ln'

    for p in pkgs:
        res = run(['sudo', op, p, repo])
        if res.returncode is not 0: sys.exit(res.returncode)

def sync_repo(repo='/var/lib/solbuild/local'):
    """Indexes and updates the local repo."""
    os.chdir(repo)
    res = run(['sudo', 'eopkg', '--skip-signing', 'index'])
    if res.returncode is not 0: sys.exit(res.returncode)
    res = run(['sudo', 'eopkg', 'update-repo'])
    if res.returncode is not 0: sys.exit(res.returncode)

def make(local=False):
    ""

    info("-- Building package:\n"
         f"-- {os.getcwd().split('/')[-1]}")

    res = run(['make'] + (['local'] if local else []))
    if res.returncode is not 0: sys.exit(res.returncode)

def build_all(rebuild=False):
    "Builds all top-level packages, starting with those with no local dependencies."
    packages, ignored, local_deps = index_pkgs()
    if len(packages) == 0:
        print('No packages found!')
        sys.exit(2)

    for p in packages:
        os.chdir(p)
        if check_build():
            if len(list_eopkgs()) == 0 or rebuild: make()
            place_in_repo()
        cache_build()
        os.chdir(os.pardir)

    for p, deps in local_deps.items():
        if p in ignored: continue
        if any(dep in ignored for dep in deps):
            info('Ignoring ' + p + ' (missing dependencies)')
            continue
        os.chdir(p)
        if check_build():
            if len(list_eopkgs()) == 0 or rebuild: make(local=True)
            place_in_repo()
        cache_build()
        os.chdir(os.pardir)

    info('Syncing')
    sync_repo()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Build tool for local repo.')
    parser.add_argument('-a', '--all', action='store_true', help='Build all packages')
    parser.add_argument('-r', '--rebuild', action='store_true', help='Rebuild build packages')

    if len(sys.argv) == 1: parser.print_help()
    else:
        try:
            args = vars(parser.parse_args())
            if args['all']:
                build_all(args['rebuild'])
            else:
                parser.print_help()
        except KeyboardInterrupt:
            print('process killed')
            sys.exit(1)
