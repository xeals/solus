# Chapel
Not particularly well supported for packaging by upstream. With the patches (in stable releases starting 1.16, ~ Oct '17) it will compile and fragment properly, but the `ypkg` binary stripper takes out the static dependencies in `third-party` that the compiler requires.
