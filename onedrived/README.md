Solus doesn't currently package most of the Python modules required for this. See the [package file](package.yml) for details. Unavailable modules are commented.
