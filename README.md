# solus
Collection of packages that haven't been submitted to the repos. As most of them are attempts at getting them to build on Solus, a few do not build:

- `onedrived` refuses to recognise the current `setuptools`;

Some workarounds had to be taken in getting them to build:
- `ttfautohint` does not come with the GUI, as the "test QT application" in the configure script fails to build.

# Local version bumps

- haskell-optparse-applicative -> 0.14.0 for haskell-tasty
- haskell-src-exts -> 1.20.1 for haskell-hindent

# elm
| !  | removed                        |
| ?  | not done due to version issues |
| #? | other issues                   |

- elm-make
    - elm-compiler
    - elm-reactor
    - haskell-raw-strings-qq
- elm-reactor
    - elm-compiler
    - elm-package
    - haskell-cmdargs
    - haskell-snap-core
        - haskell-io-streams
        - haskell-readable
        - haskell-bytestring-builder
    - haskell-snap-server
        - haskell-io-streams
        - haskell-io-streams-haproxy
        - haskell-snap-core
        - haskell-bytestring-builder
    - haskell-websockets
        - haskell-entropy
    - haskell-websockets-snap
        - haskell-io-streams
        - haskell-snap-core
        - haskell-snap-server
        - haskell-websockets
        - haskell-bytestring-builder
- elm-compiler
    - ? haskell-aeson
        - depends on < 0.8, have 0.8.5
    - ? haskell-binary
        - depends on < 8.0, current is 8.5.1
    - haskell-edit-distance
    - haskell-indents
    - haskell-language-ecmascript
        - haskell-charset
        - haskell-Diff
        - haskell-testing-feat
            - haskell-tagshare
        - haskell-wl-pprint
    - haskell-language-glsl
        - haskell-prettyclass
    - haskell-union-find
- elm-repl
    - elm-compiler
    - elm-package
    - haskell-cmdargs
    - haskell-bytestring-trie
- elm-package
    - elm-compiler
    - ? haskell-HTTP
        - depends on < 4000.3, have 4000.3.3
    - ? haskell-binary
        - depends on < 8.0, current is 8.5.1
    - haskell-edit-distance
    - ? haskell-http-client
        - depends on < 0.5, have 0.5.6.1
    - ? haskell-http-client-tls
        - depends on == 0.2.*, have 0.3.4
    - ? haskell-http-types
        - depends on < 0.9, have 0.9.1
    - haskell-parallel-io
    - haskell-zip-archive
